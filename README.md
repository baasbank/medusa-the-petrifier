## THIS README
This README contains information relevant to building the docker image, building on Gitlab, and deploying to Google Kubernetes Engine(GKE).  
The docs for the other stuff are in the respective folders' README.

### LIBERTY TAKEN
- Everything is pushed directly to the master branch. Some branching convention would of course be followed on a serious project.


### TO BUILD THE IMAGE
- Run the following commands on your terminal
 ```
 git clone git@gitlab.com:baasbank/medusa-the-petrifier.git
 cd medusa-the-petrifier
 docker build --build-arg LITECOIN_VERSION=0.18.1 -t ${username}/litecoin:0.18.1 .
 ```

 ### TO RUN A CONTAINER WITH THE IMAGE
 - Run the following on your terminal
 ```
  docker container run --rm ${username}/litecoin:0.18.1
 ```
 The above command deletes the container once it is stopped.

 WHERE:
  ${username} is your docker hub username or the url to your container registry, e.g. gcr.io/medusa-the-petrifier


### BUILDING ON GITLAB AND DEPLOYING TO A GKE CLUSTER
- Create a repo on Gitlab. 
- On Google Cloud Platform (GCP), create a service account with at least the 'roles/container.developer' role, so it can be used to deploy to GKE. Ensure to create/download the service account key file.
- Move the file to the root of this directory. Make sure to add the file to .gitignore to avoid pushing it to source control.
- Encode the content of the file using the command below
```
base64 -i ${filename.json} -o ${encoded-filename}.json
```
WHERE:
  ${filename.json} is your key file name, and  
  ${encoded-filename.json} is the name you want to give the output file from the encoding. Ensure to also add this file to .gitignore
- On the main page of the repo created in step 1, go to SETTINGS > CI/CD > VARIABLES.  
Click on 'Add Variable'.  
Use GCP_SERVICE_ACCOUNT as key, and paste in the content of <encoded-filename>.json as value.
Select the 'Mask Variable' checkbox, then 'Add Variable'.
- Update the value of the GCP_PROJECT_ID and GKE_CLUSTER_NAME variables in .gitlab-ci.yml.
- Update the value of image in kubernetes/statefulset.yaml depending on what container registry you're using.
- Push this directory to the gitlab repo created in step 1.
- Gitlab should build and deploy your image to your specified GKE cluster if all went well.

### TOOLS/SERVICES USED
- Docker/docker-cli
- GCP/GKE
- Gitlab/Gitlab CI/CD  
  

# IMPORTANT
- Both the 'Dockerfile' and 'entrypoint.sh' were copied verbatim from [here](https://github.com/uphold/docker-litecoin-core/tree/master/0.18). No need reinventing the wheel.  


### OTHER REFERENCES
https://www.shellhacks.com/gitlab-ci-cd-build-docker-image-push-to-registry/  
https://kartaca.com/en/deploy-your-application-directly-from-gitlab-ci-to-google-kubernetes-enginegke/

