provider "google" {
 credentials = file("../medusa.json")
 project     = var.project
 region      = var.region
}

# Use the project-iam module
module "iam" {
  source = "./module/project-iam"
}
