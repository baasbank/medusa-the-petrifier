#!/bin/bash

# When using a storage bucket as a backend for storing state, Terraform expects that bucket
# to be already created before running Terraform. This script makes it possible to automate the whole setup.


project_id=$1
bucket_name=$2
bucket_location=$3

randomized_bucket_name="${bucket_name}-${RANDOM}" # adds random number to desired bucket name for uniqueness
  
gsutil mb -p $project_id -c STANDARD -l $bucket_location -b on gs://$randomized_bucket_name

export TF_VAR_bucketname=$randomized_bucket_name

sed "s/TF_VAR_bucketname/\""$randomized_bucket_name"\"/g" backend.tf.tpl > backend.tf #MacOS' version of sed doesn't
# recognize the -i command. This would have invalidated the need for a template file, and done inline editing instead.

terraform init -input=false
terraform plan -no-color > planfile-humans # Show a human-readable version of the latest plan
terraform plan -out=planfile -input=false
terraform apply -input=false planfile