# NO USER AND GROUP?
You'll notice that the module in terraform/module/project-iam only contains resources for a custom role and a policy.
This is because Google's concept of user groups is a bit different from AWS' and a bit more demanding to set up. For example,
one requirement is having the domain that'll form the group/user email.  
I have simply bound one user (myself) to the policy.
I would do it on AWS but I don't have a functional AWS account because AWS wouldn't accept my card the last time I tried.

## SETUP
- Delete backend.tf in terraform/
- Make sure you're in the terraform/ directory by running ``` cd terraform``` from the root of this project.
- Make sure the setup.sh script is executable by running ```chmod +x setup.sh```.
- Make sure you're logged in to the gcloud cli.
- Run ```./setup.sh```
The script creates the storage bucket for the state, the backend.tf file, human/machine-readable plan files, and applies it.
