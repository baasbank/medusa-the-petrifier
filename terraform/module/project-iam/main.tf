resource "google_project_iam_custom_role" "prod-ci-role" {
  role_id     = "prodCIRole" # only camelCase permitted
  title       = "prod-ci-role"
  description = "A custom role"
  permissions = ["iam.roles.list"] # permissions is a required field
}

resource "google_project_iam_policy" "project" {
  project     = var.project
  policy_data = data.google_iam_policy.admin.policy_data
}

data "google_iam_policy" "admin" {
  binding {
    role = "projects/${var.project}/roles/prodCIRole"

    members = var.prod_ci_role_members
  }
}
