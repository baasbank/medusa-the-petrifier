variable "prod_ci_role_members" {
  type = list(string)
  default = ["user:baasbank.akinmuleya@ingka.ikea.com"]
}

variable "project" {
  type = string
  default = "ingka-baaki-sandbox-dev"
}
