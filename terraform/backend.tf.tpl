/*
This is a template file that setup.sh uses
to prepare backend.tf, so the whole setup can be automated.
*/
terraform {
  backend "gcs" {
    bucket  = TF_VAR_bucketname
  }
}